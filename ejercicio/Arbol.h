#include <iostream>

#include "programa.h"

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
	private:
	
	public:
		Arbol();
		Nodo* crear_nodo(int numero, Nodo *padre);
		void insertar(Nodo *&raiz, int numero, bool &flag, Nodo *padre);
		void eliminar(Nodo *&raiz, bool &flag, int numero);
		void reestructuracion_izq(bool flag, Nodo *&raiz);
		void rotacion_DD(Nodo *&raiz, bool flag, Nodo *nodo1);
		void rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2);
		void reestructuracion_der(bool flag, Nodo *&raiz);
		void rotacion_II(Nodo *&raiz, bool flag, Nodo *nodo1);
		void rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2);
};
#endif


