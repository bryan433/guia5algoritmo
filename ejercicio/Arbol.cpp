#include <iostream>
//definicion de la clase
#include "Arbol.h"

using namespace std;

//costructor
Arbol::Arbol(){

}

//creamos un nuevo nodo
Nodo* Arbol::crear_nodo(int numero, Nodo*padre){
	Nodo* tmp;
	tmp = new Nodo;
	
	// se le asigna un valor al nodo
	tmp->dato = numero;
	
	// se crean las ramas izquierda y derecha
	tmp->izq = NULL;
	tmp->der = NULL;
	tmp->nodo_padre = padre;
	
	return tmp;
}

//rotacion de la rama derececha
void Arbol::rotacion_DD(Nodo *&raiz, bool flag, Nodo *nodo1 ){
	raiz->der = nodo1->izq;
	nodo1->izq = raiz;
	
	if (nodo1->factor_equilibrio == 0){
		raiz->factor_equilibrio = 1;
		nodo1->factor_equilibrio = -1;
		flag = false;
	}
	
	else if (nodo1->factor_equilibrio == 1){
		raiz->factor_equilibrio = 0;
		nodo1->factor_equilibrio = 0;
	}
	
	raiz = nodo1;
}

void Arbol::rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
	nodo2 = nodo1->izq;
	raiz->der = nodo2->izq;
	nodo2->izq = raiz;
	nodo1->izq = nodo2->der;
	nodo2->der = nodo1;
	
	if (nodo2->factor_equilibrio == 1){
		raiz->factor_equilibrio = -1;
	}
	else{
		raiz->factor_equilibrio = 0;
	}
	
	if (nodo2->factor_equilibrio == -1){
		nodo1->factor_equilibrio = 1;
	}
	else{
		nodo1->factor_equilibrio = 0;
	}
	
	raiz = nodo2;
	nodo2->factor_equilibrio = 0;
	
}

void Arbol::reestructuracion_izq(bool flag, Nodo *&raiz){
	Nodo *nodo1;
	Nodo *nodo2;
	
	if (flag == true){
		if(raiz->factor_equilibrio == -1){
			raiz->factor_equilibrio = 0;
		}
		else if (raiz->factor_equilibrio == 0){
			raiz->factor_equilibrio = 1;
			flag = false;
		}
		else if (raiz->factor_equilibrio == 1){
			nodo1 = raiz->der;
			
			if (nodo1->factor_equilibrio >=0){
				rotacion_DD(raiz, flag, nodo1);
			}
			else{
				rotacion_DI(raiz, nodo1, nodo2);
			}
		}
	}				
}

void Arbol::rotacion_II(Nodo *&raiz, bool flag, Nodo *nodo1){
	raiz->izq = nodo1->der;
	nodo1->der = raiz;
	
	if (nodo1->factor_equilibrio == 0){
		raiz->factor_equilibrio = -1;
		nodo1->factor_equilibrio = 1;
		flag = false;
	}
	
	else if (nodo1->factor_equilibrio == -1){
		raiz->factor_equilibrio = 0;
		nodo1->factor_equilibrio = 0;
	}
	
	raiz = nodo1;
}

void Arbol::rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
	nodo2 = nodo1->der;
	raiz->izq = nodo2->der;
	nodo2->der =raiz;
	nodo1->der = nodo2->izq;
	nodo2->izq = nodo1;
	
	if (nodo2->factor_equilibrio == -1){
		raiz->factor_equilibrio = 1;
	}
	
	else{
		raiz->factor_equilibrio = 0;
	}
	
	if (nodo2->factor_equilibrio == 1){
		nodo1->factor_equilibrio = -1;
	}
	else{
		nodo1->factor_equilibrio = 0;
	}
	
	raiz = nodo2;
	nodo2->factor_equilibrio = 0;
}

void Arbol::reestructuracion_der(bool flag, Nodo *&raiz){
	Nodo *nodo1;
	Nodo *nodo2;
	
	if (flag == true){
	
		if (raiz->factor_equilibrio == 1){
			raiz->factor_equilibrio = 0;
		}
		else if (raiz->factor_equilibrio == 0){
			raiz->factor_equilibrio = -1;
			flag = false;
		}
		else if (raiz->factor_equilibrio == -1){
			nodo1 = raiz->izq;
			if (nodo1->factor_equilibrio <= 0){
				rotacion_II(raiz, flag, nodo1);
			}
			else{
				rotacion_ID(raiz, nodo1, nodo2);
			}
		}
	}
}


void Arbol::insertar(Nodo *&raiz, int numero, bool &flag, Nodo *padre){
	int n;
	
	if (raiz == NULL){
		Nodo *tmp;
		tmp = crear_nodo(numero, padre);
		raiz = tmp;
		insertar(raiz, numero, flag, padre);
	}
	else{
		n = raiz->dato;
		
		if (numero < n){
			insertar(raiz->izq, numero, flag, raiz);
			reestructuracion_der(flag, raiz);
		}
		
		else if (numero > n){
			insertar(raiz->der, numero, flag, raiz);
			reestructuracion_izq(flag, raiz);
		}
		else{
			cout << numero << " no se puede repetir el numero" << endl;
		}
	}
	flag = true;
}


void Arbol::eliminar (Nodo *&raiz, bool &flag, int numero){
	Nodo *tmp;
	Nodo *nodo1;
	Nodo *nodo2;
	bool bandera;
	
	if (raiz !=NULL){
		if (numero < raiz->dato){
			eliminar(raiz->izq, flag, numero);
			reestructuracion_izq(flag, raiz);
		}
		
		else if (numero > raiz->dato){
			eliminar(raiz->der, flag, numero);
			reestructuracion_der(flag, raiz);
		}
		
		else{
			tmp = raiz;
            // Subárbol derecho vacio
            if(tmp->der == NULL){
                raiz = tmp->izq;
            }
            
            else{
                // Subárbol izquierdo vacio
                if(tmp->izq == NULL){
                    raiz = tmp->der;
                }
                
                else{ // Elementos con dos descendientes
                    nodo1 = raiz->izq;
                    bandera = false;
                   
					while (nodo1->der !=NULL){
						nodo2 =nodo1;
						nodo1 = nodo1->der;
						bandera = true;
					}
				
					raiz->dato = nodo1->dato;
					tmp = nodo1;
				
					if (bandera == true){
						nodo2->der = nodo1->izq;
					}
					else{
						raiz->izq = nodo1->izq;
						free(tmp); // libera la memoria
					}	
				}
			}
		}
	}
	else{
		cout << "el numero ingresado no se encuentra en el arbol";
	}
}  
