#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include "Arbol.h"

using namespace std;

#include <unistd.h>

class Graficar {
	private:
		Nodo *arbol =NULL;
		
	public:
	
		Graficar(Nodo *raiz){
			this->arbol = raiz;
		}
		
		
		 void recorrerArbol(Nodo *p, ofstream &archivo) {
	        string infoTmp;
	        /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador*/
	        if (p != NULL) {
	        // Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo.
	        if (p->izq != NULL) {
		        archivo<< p->dato << "->" << p->izq->dato << "[label=" << p->factor_equilibrio << "];" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "i";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }
	        
	        infoTmp = p->dato;
	        
	        if (p->der != NULL) {
		        archivo << p->dato << "->" << p->der->dato << "[label=" << p->factor_equilibrio << "];" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "d";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }
	        
	        // Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo.
	        recorrerArbol(p->izq, archivo);
	        recorrerArbol(p->der, archivo);
	        }

	        return;
        }
		
		void crearGraficar() {
	        ofstream archivo;
	        // Se abre/crea el archivo datos.txt, a partir de este se generará el grafo.
	        archivo.open("datos.txt");
	        // Se escribe dentro del archivo datos.txt "digraph G { ".
	        archivo << "digraph G {" << endl;
	        // Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde.
	        archivo << "node [style=filled fillcolor=cyan];" << endl;
	        // Llamado a la función recursiva que genera el archivo de texto para creación del grafo.
	        recorrerArbol(this->arbol, archivo);
	        // Se termina de escribir dentro del archivo datos.txt.
	        archivo << "}" << endl;
	        archivo.close();
	        
	        // Genera el grafo.
	        system("dot -Tpng -ografo.png datos.txt &");
	        system("eog grafo.png &");
        }
};


int ingreso_numero(){
	int numero;
	system("clear");
	cout << "ingrese un numero: ";
	cin >> numero;
	
	return numero;
}

void ingresar_nodo(Arbol *nuevo_arbol, Nodo *&raiz, bool &flag){
	Nodo *nodo = NULL;
	int numero = ingreso_numero();
	
	nodo = nuevo_arbol->crear_nodo(numero, NULL);
	
	if (raiz == NULL){
		raiz= nodo;
	}
	
	else{
		nuevo_arbol->insertar(raiz, numero, flag, NULL);
	}

}

void menu (Arbol *nuevo_arbol, Nodo *raiz){
	int opcion;
	bool flag = false;
	int numero;
	
	do{
		
		cout << "Menu" << endl;
		cout << "[1]  Ingresar numero" << endl;
		cout << "[2]  eliminar algun nodo" << endl;
		cout << "[3]  Modificar nodo" << endl;
		cout << "[4]  Mostrar grafo" << endl;
		cout << "[5]  Salir" << endl;
		
		cout << "Ingrese alguna opcion: ";
		cin >> opcion;
		
		switch(opcion){
			case 1:
				ingresar_nodo(nuevo_arbol, raiz, flag);
				break;
			
			case 2:
				numero = ingreso_numero();
				nuevo_arbol->eliminar(raiz, flag, numero);
				break;
				
			case 3:
				numero = ingreso_numero();
				nuevo_arbol->eliminar(raiz, flag, numero);
				ingresar_nodo(nuevo_arbol, raiz, flag);
				
			case 4:
				Graficar *g = new Graficar(raiz);
				g->crearGraficar();
				break;
			}
			system("clear");
		}
		while(opcion != 5);
		} 


int main(){
	Arbol *nuevo_arbol = new Arbol();
	Nodo *raiz = NULL;
	
	menu(nuevo_arbol, raiz);
	
	return 0;
}
